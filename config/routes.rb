Rails.application.routes.draw do
  get 'report/index'

	resources :survivors, except:  [:destroy] do
		resources :resources, only:  [:index]
		resources :resources_trade , only: [:index,:create]

		match 'flaginfects', to: 'survivors#reportInf', via: [:get]
		match 'updatelocation', to: 'survivors#lastposition', via: [:post]

	end

	resources :trades, only: [:index, :create]
	match 'reports', to: 'reports#index', via: [:get]

	root to: "survivors#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
