require_relative '../config/environment'

describe "Assignment" do

    before :all do
        $continue = true
    end

    around :each do |example|
        if $continue
            $continue = false 
            example.run 
            $continue = true unless example.exception
        else
            example.skip
        end
    end

    #helper method to determine if Ruby class exists as a class
    def class_exists?(class_name)
      eval("defined?(#{class_name}) && #{class_name}.is_a?(Class)") == true
    end

    #helper method to determine if two files are the same
    def files_same?(file1, file2) 
      if (File.size(file1) != File.size(file2)) then
        return false
      end
      f1 = IO.readlines(file1)
      f2 = IO.readlines(file2)
      if ((f1 - f2).size == 0) then
        return true
      else
        return false
      end
    end

    context "rq01" do
      # Test to make sure model objects exist and are ActiveRecord objects
      context "Generate two(2) Model Classes and Properties" do
        context "rq01.1 Survivor Model" do
          it "Survivor class implemented" do
            expect(class_exists?("Survivor"))
            expect(Survivor < ActiveRecord::Base).to eq(true)
          end
          context "Survivor class properties defined" do
            subject(:survivors) { Survivor.new }
            it { is_expected.to respond_to(:name) } 
            it { is_expected.to respond_to(:age) } 
            it { is_expected.to respond_to(:gender) }
            it { is_expected.to respond_to(:lat) }
            it { is_expected.to respond_to(:long) }
            it { is_expected.to respond_to(:created_at) } 
            it { is_expected.to respond_to(:updated_at) } 
          end
          it "Survivor database structure in place" do
            expect(Survivor.column_names).to include "name", "age", "gender", "lat", "long"
            expect(Survivor.column_for_attribute('name').type).to eq :string
            expect(Survivor.column_for_attribute('age').type).to eq :integer
            expect(Survivor.column_for_attribute('gender').type).to eq :string
            expect(Survivor.column_for_attribute('lat').type).to eq :float
            expect(Survivor.column_for_attribute('long').type).to eq :float
            expect(Survivor.column_for_attribute('created_at').type).to eq :datetime
            expect(Survivor.column_for_attribute('updated_at').type).to eq :datetime
          end
        end

        context "rq01.2 Resource Model" do
          it "Resource class implemented" do
            expect(class_exists?("Resource"))
            expect(Resource < ActiveRecord::Base).to eq(true)
          end
          context "Resources class properties defined" do
            subject(:resources) { Resource.new }
            it { is_expected.to respond_to(:water) } 
            it { is_expected.to respond_to(:food) } 
            it { is_expected.to respond_to(:medication) } 
            it { is_expected.to respond_to(:ammunition) } 
            it { is_expected.to respond_to(:created_at) } 
            it { is_expected.to respond_to(:updated_at) } 
          end
          it "Resource database structure in place" do
            expect(Resource.column_names).to include "water", "food", "medication", "ammunition"
            expect(Resource.column_for_attribute('water').type).to eq :integer
            expect(Resource.column_for_attribute('food').type).to eq :integer
            expect(Resource.column_for_attribute('medication').type).to eq :integer
            expect(Resource.column_for_attribute('ammunition').type).to eq :integer
            expect(Survivor.column_for_attribute('created_at').type).to eq :datetime
            expect(Survivor.column_for_attribute('updated_at').type).to eq :datetime
          end
        end
        
        context "rq01.3   the relationship declared  in  the model class " do
          
          it "Survivor 1:1 has_one relationship to Resource in place" do
            expect(Survivor.reflect_on_association(:resource).macro).to eq :has_one
          end
          it "Survivor 1:1 has_one relationship to Resource in place" do
            expect(Resource.reflect_on_association(:survivor).macro).to eq :belongs_to
          end
        end

      end
    end

end #end Assignment