class CreateTrades < ActiveRecord::Migration[5.1]
  def change
    create_table :trades do |t|
      t.references :survivor1
      t.references :survivor2
      t.timestamps
    end
  end
end
