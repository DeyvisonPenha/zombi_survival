class ResourcesController < ApplicationController
  before_action :set_survivor_resource, only: [:index, :update, :create]

  # GET /resources
  def index
    @resource ||= @current_surv.resource

    render json: @resource
  end

  # PATCH/PUT /resources/1
  # def update
  #   if @resource.update(resource_params)
  #     render json: @resource
  #   else
  #     render json: @resource.errors, status: :unprocessable_entity
  #   end
  # end

  # DELETE /resources/1
  #def destroy
   # @resource.destroy
  #end

  private
    
    # Only allow a trusted parameter "white list" through.
    def resource_params
      params.require(:resource).permit(:water, :food, :medication, :ammunition, :survivor_id)
    end
end
