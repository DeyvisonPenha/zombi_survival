class Trade < ApplicationRecord
	
	belongs_to :survivor1, class_name: 'Survivor'
	belongs_to :survivor2, class_name: 'Survivor'

	validate :verify_trade

	attr_accessor :name, :age, :gender, :lat, :long, :report

	# survivor1 is the trader and survivor2 is the buyer

	def self.points(param) # param have to be survivor1.id
		res_trad = Survivor.find(param).resource_trade.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact
		res = Survivor.find(param).resource.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact
		
		# check if the trader has the resources to trade and calculates the points
		res.each do |key,value| 
			res[key] = value-res_trad[key].to_i 
			if res[key].negative?
				self.errors.add(:trade, "survivor dont have resources to trade")
			end
		end
		@s_res = Survivor.find(param).resource_trade
		@point = @s_res.water.to_i*4+@s_res.food.to_i*3+@s_res.medication.to_i*2+@s_res.ammunition.to_i
	end

	# check if both survivors have points and resources to trade
	def checkpoint
		if Trade.points(survivor1.id) != Trade.points(survivor2.id)
			self.errors.add(:trade, "not points equal. survivor did not follow the rules for trade")
			return true
		else
			return false
		end	
	end		


	def checkinfection
		if survivor1.report ==3 
			self.errors.add(:trade, "survivor #{survivor1.name} is infected. survivor did not follow the rules for trade")
			return true
		end

		if survivor2.report ==3
			self.errors.add(:trade, "survivor #{survivor2.name} is infected. survivor did not follow the rules for trade")
			return true
		end
		
	end


	private

	def verify_trade

		if survivor1.id == survivor2.id or checkpoint or checkinfection
			#self.errors.add(:trade, "survivor did not follow the rules for trade")
		else
			swap
		end

	end

	def swap
		res_trad = Survivor.find(survivor1.id).resource_trade.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact
		res_buyer = Survivor.find(survivor2.id).resource_trade.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact

		res_surv1 = Survivor.find(survivor1.id).resource.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact
		res_surv2 = Survivor.find(survivor2.id).resource.as_json({except: [:created_at, :updated_at, :survivor_id, :id]}).compact

		res_surv2.each do |key,value| 
			res_surv2[key] = value-res_buyer[key].to_i + res_trad[key].to_i 
		end

		res_surv1.each do |key,value| 
			res_surv1[key] = value + res_buyer[key].to_i - res_trad[key].to_i 
		end
		
		Survivor.find(survivor2.id).resource.update(res_surv2.to_h)
		Survivor.find(survivor1.id).resource.update(res_surv1.to_h)

	end
	
end