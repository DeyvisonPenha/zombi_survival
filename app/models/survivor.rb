class Survivor < ApplicationRecord
	has_one :resource
	has_one :resource_trade
	has_many :primary_trades, :class_name => "Trade", :foreign_key => "survivor1_id"
  	has_many :secondary_trades, :class_name => "Trade", :foreign_key => "survivor2_id"

  	#accepts_nested_attributes_for :resource


end
